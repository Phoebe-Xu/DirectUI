#pragma once
#include "RenderTarget.h"
class CUIControl : public IUIControl
{
public:
	CUIControl();
	virtual ~CUIControl();

public:
	std::wstring		Name();
	void				Name(const std::wstring& strName);

	RECT				Padding();
	void				Padding(const RECT& rcPadding);
	SIZE				Size();
	void				Size(SIZE szCtrl);
	RECT				TextPadding();
	void				TextPadding(const RECT& rcTextPadding);

	std::wstring		Text();
	void				Text(const std::wstring& strText);
	DWORD				TextColor();
	void				TextColor(DWORD dwColor);

	bool				Visible();
	void				Visible(bool bVisible);
	bool				Focus();
	void				Focus(bool bFocus);
	bool				Enabled();
	void				Enabled(bool bEnabled);
	bool				MouseEnabled();
	void				MouseEnabled(bool bEnabled);
	bool				KeyboardEnabled();
	void				KeyboardEnabled(bool bEnabled);

	DWORD				BkColor();
	void				BkColor(DWORD dwColor);
	std::wstring		BkImage();
	void				BkImage(const std::wstring& strPath);

	DWORD				BorderColor();
	void				BorderColor(DWORD dwColor);
	DWORD				FocusBorderColor();
	void				FocusBorderColor(DWORD dwColor);
	RECT				BorderSize();
	void				BorderSize(const RECT& rcSize);
	int					BorderRound();
	void				BorderRound(int nBorderRound);


public:
	CUIControl*			Parent();
	void				Parent(CUIControl* pUIControl);
	SIZE				CalcSize();
	void				CalcSize(SIZE szCtrl);

	void				LockUIUpdate();
	void				UnlockUIUpdate();

	virtual bool		Paint(CRenderTarget* pRenderTarget, const RECT& rcPaint);
	virtual void		PaintBkColor(CRenderTarget* pRenderTarget);
	virtual void		PaintBkImage(CRenderTarget* pRenderTarget);
	virtual void		PaintText(CRenderTarget* pRenderTarget);
	virtual void		PaintBorder(CRenderTarget* pRenderTarget);

protected:
	// 通过配置文件直接设置的参数
	std::wstring		m_strName;				// 控件的名字

	RECT				m_rcPadding;			// 在父容器中距离临近的控件间隔(left, top, right, bottom)
	SIZE				m_szCtrl;				// 配置文件中设置的宽高尺寸(当父容器中可用空间不足或未设置此值时，会自动计算m_szCalcSize)
	RECT				m_rcTextPadding;		// 文本相对控件偏移(left, top, right, bottom)m_rcTextPadding 在去掉 m_rcPadding 间隔后的中间区域

	std::wstring		m_strText;				// 控件显示的文本
	DWORD				m_dwTextColor;			// 文本颜色
	
	bool				m_bVisible;				// 是否可见
	bool				m_bFocus;				// 当前是否获得焦点
	bool				m_bEnabled;				// 是否是可用的(normal hover pressed disabled)
	bool				m_bMouseEnabled;		// 是否可通过鼠标操作(Label等不可通过鼠标操作)
	bool				m_bKeyboardEnabled;		// 是否可通过键盘操作(Button按Enter, CheckBox空格选中或取消选中)

	/*
	 先填充背景色，再绘制背景图。
	 图片路径有可能是这样的 bkimage="file='xxxxx.png' source='0,0,100,22' dest='10,10,50,11'"
	*/
	DWORD				m_dwBkColor;			// 背景色
	std::wstring		m_strBkImage;			// 背景图
	
	DWORD				m_dwBorderColor;		// 边框颜色
	DWORD				m_dwFocusBorderColor;	// 获得焦点后的边框颜色
	RECT				m_rcBorderSize;			// 边框线宽(left, top, right, bottom)
	int					m_nBorderRound;			// 圆角直径


protected:
	// 库内部使用的参数
	CUIControl*			m_pParent;				// 父控件
	SIZE				m_szCalcSize;			// 在没有设置m_szCtrl的情况下，自动计算的尺寸，这个尺寸会变化。

	CRenderTarget*		m_pRenderTarget;
	bool				m_bLockUIUpdate;
};

