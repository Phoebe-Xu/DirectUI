#pragma once
class CRenderTarget
{
public:
	CRenderTarget();
	virtual ~CRenderTarget();

public:
	void	Invalidate(IUIControl* pUIControl);
};

