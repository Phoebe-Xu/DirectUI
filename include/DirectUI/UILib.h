#pragma once
#include <windows.h>
#include <iostream>
#include <vector>

#include "IUIControl.h"
#include "RenderTarget.h"

#include "Control/UIControl.h"

#include "Layout/UIContainer.h"
#include "Layout/UIHorizontalLayout.h"
#include "Layout/UIVerticalLayout.h"
