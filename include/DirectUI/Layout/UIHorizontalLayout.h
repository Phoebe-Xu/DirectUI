#pragma once
class CUIHorizontalLayout : public CUIControl
{
public:
	CUIHorizontalLayout();
	virtual ~CUIHorizontalLayout();

public:
	int32_t			AddItem(CUIControl* pUIControl, int32_t nIndex = -1);
	void			RemoveItem(CUIControl* pUIControl);
	void			RemoveItem(int32_t nIndex = -1);	// 当 0 > nIndex的时候移除所有子项
	CUIControl*		GetItem(uint32_t nIndex);

protected:
	std::vector<CUIControl*>	m_vecUIControl;
};

