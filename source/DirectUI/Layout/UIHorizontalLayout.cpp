#include "stdafx.h"
#include "UIHorizontalLayout.h"


CUIHorizontalLayout::CUIHorizontalLayout()
{
}

CUIHorizontalLayout::~CUIHorizontalLayout()
{
}

int32_t CUIHorizontalLayout::AddItem(CUIControl* pUIControl, int32_t nIndex /*= -1*/)
{
	if (nullptr == pUIControl)
	{
		nIndex = -1;
	}
	else if (0 <= nIndex && size_t(nIndex) < m_vecUIControl.size())
	{
		m_vecUIControl.insert(m_vecUIControl.begin() + nIndex, pUIControl);
	}
	else
	{
		m_vecUIControl.push_back(pUIControl);
		nIndex = m_vecUIControl.size();
	}
	return nIndex;
}

void CUIHorizontalLayout::RemoveItem(CUIControl* pUIControl)
{
	if (nullptr != pUIControl)
	{
		std::vector<CUIControl*>::iterator it = std::find(m_vecUIControl.begin(), m_vecUIControl.end(), pUIControl);
		if (m_vecUIControl.end() != it)
		{

		}
	}
}

void CUIHorizontalLayout::RemoveItem(int32_t nIndex /*= -1*/)
{

}

CUIControl* CUIHorizontalLayout::GetItem(uint32_t nIndex)
{
	CUIControl* pUIControl = nullptr;
	if (0 <= nIndex && (size_t)nIndex < m_vecUIControl.size())
	{
		pUIControl = m_vecUIControl[nIndex];
	}
	return pUIControl;
}
