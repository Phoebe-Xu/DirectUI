#include "stdafx.h"
#include <UIControl.h>


CUIControl::CUIControl()
{
	RtlZeroMemory(&m_rcPadding, sizeof(RECT));
	RtlZeroMemory(&m_szCtrl, sizeof(SIZE));
	RtlZeroMemory(&m_rcTextPadding, sizeof(RECT));

	m_dwTextColor = 0x00FFFFFF;

	m_bVisible = true;
	m_bFocus = false;
	m_bEnabled = true;
	m_bMouseEnabled = true;
	m_bKeyboardEnabled = false;

	m_dwBkColor = 0x00FFFFFF;

	m_dwBorderColor = 0x00FFFFFF;
	m_dwFocusBorderColor = 0x00FFFFFF;
	RtlZeroMemory(&m_rcBorderSize, sizeof(RECT));
	m_nBorderRound = 2;

	m_pParent = nullptr;
	RtlZeroMemory(&m_szCalcSize, sizeof(SIZE));

	m_pRenderTarget = nullptr;
	m_bLockUIUpdate = false;
}

CUIControl::~CUIControl()
{

}

std::wstring CUIControl::Name()
{
	return m_strName;
}

void CUIControl::Name(const std::wstring& strName)
{
	m_strName = strName;
}

RECT CUIControl::Padding()
{
	return m_rcPadding;
}

void CUIControl::Padding(const RECT& rcPadding)
{
	m_rcPadding = rcPadding;
}

SIZE CUIControl::Size()
{
	return m_szCtrl;
}

void CUIControl::Size(SIZE szCtrl)
{
	m_szCtrl = szCtrl;
}

RECT CUIControl::TextPadding()
{
	return m_rcTextPadding;
}

void CUIControl::TextPadding(const RECT & rcTextPadding)
{
	m_rcTextPadding = rcTextPadding;
}

std::wstring CUIControl::Text()
{
	return m_strText;
}

void CUIControl::Text(const std::wstring& strText)
{
	m_strText = strText;
}

DWORD CUIControl::TextColor()
{
	return m_dwTextColor;
}

void CUIControl::TextColor(DWORD dwColor)
{
	m_dwTextColor = dwColor;
}

bool CUIControl::Visible()
{
	return m_bVisible;
}

void CUIControl::Visible(bool bVisible)
{
	m_bVisible = bVisible;
}

bool CUIControl::Focus()
{
	return m_bFocus;
}

void CUIControl::Focus(bool bFocus)
{
	m_bFocus = bFocus;
}

bool CUIControl::Enabled()
{
	return m_bEnabled;
}

void CUIControl::Enabled(bool bEnabled)
{
	m_bEnabled = bEnabled;
}

bool CUIControl::MouseEnabled()
{
	return m_bMouseEnabled;
}

void CUIControl::MouseEnabled(bool bEnabled)
{
	m_bMouseEnabled = bEnabled;
}

bool CUIControl::KeyboardEnabled()
{
	return m_bKeyboardEnabled;
}

void CUIControl::KeyboardEnabled(bool bEnabled)
{
	m_bKeyboardEnabled = bEnabled;
}

DWORD CUIControl::BkColor()
{
	return m_dwBkColor;
}

void CUIControl::BkColor(DWORD dwColor)
{
	m_dwBkColor = dwColor;
}

std::wstring CUIControl::BkImage()
{
	return m_strBkImage;
}

void CUIControl::BkImage(const std::wstring& strPath)
{
	m_strBkImage = strPath;
}

DWORD CUIControl::BorderColor()
{
	return m_dwBorderColor;
}

void CUIControl::BorderColor(DWORD dwColor)
{
	m_dwBorderColor = dwColor;
}

DWORD CUIControl::FocusBorderColor()
{
	return m_dwFocusBorderColor;
}

void CUIControl::FocusBorderColor(DWORD dwColor)
{
	m_dwFocusBorderColor = dwColor;
}

RECT CUIControl::BorderSize()
{
	return m_rcBorderSize;
}

void CUIControl::BorderSize(const RECT& rcSize)
{
	m_rcBorderSize = rcSize;
}

int CUIControl::BorderRound()
{
	return m_nBorderRound;
}

void CUIControl::BorderRound(int nBorderRound)
{
	m_nBorderRound = nBorderRound;
}

CUIControl* CUIControl::Parent()
{
	return m_pParent;
}

void CUIControl::Parent(CUIControl* pUIControl)
{
	m_pParent = pUIControl;
}

SIZE CUIControl::CalcSize()
{
	return m_szCalcSize;
}

void CUIControl::CalcSize(SIZE szCtrl)
{
	m_szCalcSize = szCtrl;
}

void CUIControl::LockUIUpdate()
{
	m_bLockUIUpdate = true;
}

void CUIControl::UnlockUIUpdate()
{
	m_bLockUIUpdate = false;
	if (nullptr != m_pRenderTarget)
	{
		m_pRenderTarget->Invalidate(this);
	}
}

bool CUIControl::Paint(CRenderTarget* pRenderTarget, const RECT& rcPaint)
{
	bool bSuccess = true;
	if (nullptr != pRenderTarget)
	{
		m_pRenderTarget = pRenderTarget;
	}
	return bSuccess;
}

void CUIControl::PaintBkColor(CRenderTarget* pRenderTarget)
{

}

void CUIControl::PaintBkImage(CRenderTarget* pRenderTarget)
{

}

void CUIControl::PaintText(CRenderTarget* pRenderTarget)
{

}

void CUIControl::PaintBorder(CRenderTarget* pRenderTarget)
{

}
